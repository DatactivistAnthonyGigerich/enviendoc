# Installation Guide

## Dependencies

You will need npm to install the dependencies :

```
sudo apt install npm
```

Then, install the project dependencies :

```bash
cd my-app
npm install
```

## Development

To run the solution locally :
    
```bash
npm run serve
```

## Production

To build the solution for production :

```bash
npm run build
```

The website files will be available in the `dist` folder.